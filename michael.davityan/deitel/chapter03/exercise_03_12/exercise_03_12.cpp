#include "Account.hpp" 
#include <iostream>

int
main()
{
    int clientBalance;
    int clientCredit;
    int clientDebit;

    std::cout << "Insert balance: ";
    std::cin >> clientBalance;
    Account userAccount1(clientBalance);
    std::cout << "Insert credit: ";
    std::cin >> clientCredit;
    std::cout << "Insert debit: ";
    std::cin >> clientDebit;
    std::cout << userAccount1.credit(clientCredit) << "\n";
    std::cout << userAccount1.debit(clientDebit) << "\n";

    std::cout << "Insert balance: ";
    std::cin >> clientBalance;
    Account userAccount2(clientBalance);
    std::cout << "Insert credit: ";
    std::cin >> clientCredit;
    std::cout << "Insert debit: ";
    std::cin >> clientDebit;
    std::cout << userAccount2.credit(clientCredit) << "\n";
    std::cout << userAccount2.debit(clientDebit) << std::endl;
 
    return 0;     
}
