#include <iostream>

int
main()
{
    double radius;

    std::cin >> radius;

    if (radius < 0) {
        std::cout << "Error 1: wrong radius." << std::endl;
        return 1;
    }

    std::cout << "Circle diameter is: " << (2 * radius) << "\n"
              << "Length of circle is: " << (2 * 3.14159 * radius) << "\n"
              << "Circle аrеа is: " << (3.14159 * radius * radius) << std::endl;

    return 0;
}
