#include <iostream>

int
main()
{
    int countLimit; /// this variable is count limit variable that must user insert.

    std::cin >> countLimit; /// initialize countLimit by value.

    if (countLimit <= 0) {
        std::cout << "Error1: wrong limit value." << std::endl;
        return 1;
    }

    std::cout << "N\t" << "10*N\t" << "100*N\t" << "1000*N\t\n";

    int count = 1; /// counter variable that must be increased by one.

    while (count <= countLimit) {
        std::cout << count << "\t" << count * 10 << "\t" << count * 100 << "\t"
                  << count * 1000 << std::endl;
        ++count; 
    }

    return 0;
}
