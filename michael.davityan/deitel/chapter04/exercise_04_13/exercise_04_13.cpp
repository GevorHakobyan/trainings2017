#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    int totalMiles = 0;
    int totalGallons = 0;

    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter the miles used (-1 to quit): ";
        }

        int miles;
        std::cin >> miles;
        if (-1 == miles) {
            return 0;
        }
        if (miles < 0) {
            std::cout << "Error 1: wrong miles." << std::endl;
            return 1;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter gallons: ";
        }

        int gallons;
        std::cin >> gallons;
        if (gallons < 0) {
            std::cout << "Error 2: wrong gallons." << std::endl;
            return 2;
        }

        totalMiles += miles;
        totalGallons += gallons;
        
        std::cout << "MPG this tankful: "; 
        std::cout << std::setprecision(6) << std::fixed << static_cast<double>(miles) / gallons << "\n";
        std::cout << "Total MPG: ";
        std::cout  << std::setprecision(6) << std::fixed << static_cast<double>(totalMiles) / totalGallons 
                    << "\n" << std::endl;
    }

    return 0;
}
