#include <iostream>

int 
main()
{
    int countOfDigits;
    std::cout << "Enter first digit: ";
    std::cin >> countOfDigits;
    if (countOfDigits <= 0) {
        std::cout << "Error 1: Count of integers cannot be negatve or 0." << std::endl;
        return 1;
    }
    int smallest = 2147483647; /// int has 4 byte, so my number will be 2147483647.
    for (int i = 1; i <= countOfDigits; ++i) {
        int digit;
        std::cin >> digit;
        if (smallest > digit) {
            smallest = digit;
        }
    } 
    std::cout << "The smallest integer is " << smallest << std::endl;
    return 0;
}
