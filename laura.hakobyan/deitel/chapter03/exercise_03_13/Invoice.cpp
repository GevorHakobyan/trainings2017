#include "Invoice.hpp"
#include <string>

Invoice::Invoice(std::string partNumber, std::string partDescription, int purchaseQuantity, int pricePerItem)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setPurchaseQuantity(purchaseQuantity);
    setPricePerItem(pricePerItem);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
}

std::string 
Invoice::getPartDescription()
{
    return partDescription_;
}

void
Invoice::setPurchaseQuantity(int purchaseQuantity)
{
    if (purchaseQuantity < 0) {
        purchaseQuantity_ = 0;
        return;
    }
    
    purchaseQuantity_ = purchaseQuantity;
}
 
int 
Invoice::getPurchaseQuantity()
{
    return purchaseQuantity_;
}

void
Invoice::setPricePerItem(int pricePerItem)
{
    pricePerItem_ = pricePerItem;
}

int
Invoice::getPricePerItem()
{
    return pricePerItem_;
}

int
Invoice::getInvoiceAmount()
{ 
    int invoiceAmount_ = purchaseQuantity_ * pricePerItem_;
    return invoiceAmount_;
}

