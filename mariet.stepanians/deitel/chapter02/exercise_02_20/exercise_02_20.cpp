#include <iostream>

int
main()
{
    int radius;
    
    std::cout << "Enter the radius of a circle: ";
    std::cin  >> radius;
    
    if (0 >= radius) {
        std::cout << "Radius cannot be less then or equal to zero. Please enter a number which is bigger than 0." << std::endl;
        return 0;
    }
    
    int diametor = 2 * radius;
    int circumference = diametor * 3.14159;
    int area = radius * radius * 3.14159;
    
    std::cout << "\nThe diametor is equal to  " << diametor << std::endl;
    std::cout << "The circumference is equal to " << circumference << std::endl;
    std::cout << "The area is equal to " << area << std::endl;
    
    return 0;
}

