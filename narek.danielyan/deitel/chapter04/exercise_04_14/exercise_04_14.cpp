#include<iostream>
#include<iomanip>

int
main()
{
    float number, balance, consumption, receipt, limit;

    std::cout << "Account number(-1 to end): ";
    std::cin >> number;
    
    while (number != -1) {
        std::cout << "\nEnter beginning balance: ";
        std::cin >> balance;
        std::cout << "\nEnter total charges: ";
        std::cin >> consumption;
        std::cout << "\nEnter total credits: ";
        std::cin >> receipt;
        std::cout << "\nEnter credit limit: ";
        std::cin >> limit;
        balance = balance + consumption - receipt; 
        std::cout << "\nNew balance: " << std::setprecision(2) << std::fixed << balance << std::endl;

        if (balance > limit) {
            std::cout << "Account number: " << std::setprecision(2) << std::fixed << number << std::endl;
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << limit << std::endl;
            std::cout << "Balance: " << std::setprecision(2) << std::fixed << balance << std::endl;
            std::cout << "Credit limit exceeded" << std::endl;
        }
        std::cout << "Account number(-1 to end): ";
        std::cin >> number;
    }
    return 0;
}
            
    
