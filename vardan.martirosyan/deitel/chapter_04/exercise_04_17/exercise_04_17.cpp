#include <iostream>
#include <climits>

int
main()
{
    int counter = 1, largest = INT_MIN, numberForMax = 1;

    while (counter <= 10) {
        int number;
        std::cout << "Enter price in seller nomber " << counter << ": ";
        std::cin  >> number;
        
        if (largest < number) {
            largest = number;
            numberForMax = counter;
        }
        ++counter;
    }
    std::cout << "Max is number "  << numberForMax << ": " << largest << std::endl;
    return 0;    
}
    
