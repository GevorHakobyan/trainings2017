#include <iostream>

int
main()
{
    double sum = 0;
    int count = 0, number;

    do {
        std::cout << "Enter number (for the last number write 9999): ";
        std::cin  >> number;
        if (9999 == number) {
            break;
        }
        sum += number;
        ++count;
    } while (true); 
    std::cout << "Value is " << (sum / count) << std::endl;
    return 0;
}
