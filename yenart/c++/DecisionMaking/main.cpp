/// Comparing integers using if statements
#include <iostream> /// standard input and output
#include <unistd.h> /// for checking the interactivity

/// function main begins program execution
int
main()
{
    /// prompt user for data if in interactive mode
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter two integers to compare: ";
    }

    /// variable declarations
    int number1, number2;

    /// read two integers from user
    std::cin >> number1 >> number2;
    if (number1 == number2) {
        std::cout << number1 << " == " << number2 << std::endl;
    }
    if (number1 != number2) {
        std::cout << number1 << " != " << number2 << std::endl;
    }
    if (number1 < number2) {
        std::cout << number1 << " < " << number2 << std::endl;
    }
    if (number1 > number2) {
        std::cout << number1 << " > " << number2 << std::endl;
    }
    if (number1 <= number2) {
        std::cout << number1 << " <= " << number2 << std::endl;
    }
    if (number1 >= number2) {
        std::cout << number1 << " >= " << number2 << std::endl;
    }
    return 0;
} /// end function main
