#include "invoice.hpp"
#include <iostream>
#include <string>

int 
main()
{    
    int itemNumber = 200;
    std::string description = "Keyboard";
    int quantity = 2;
    int price = 10;

    Invoice purchase(itemNumber, description, quantity, price);

    std::cout << "Item number - " << purchase.getItemNumber() << "\n";
    std::cout << "Item description - " << purchase.getDescription() << "\n";
    std::cout << "Quantity - " << purchase.getQuantity() << "\n";
    std::cout << "Price - " << purchase.getPrice() << "\n";
    std::cout << "Total amount - " << purchase.getInvoiceAmount() << std::endl;

    return 0;
}

