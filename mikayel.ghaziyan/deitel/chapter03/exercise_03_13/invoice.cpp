#include "invoice.hpp"
#include <iostream>
#include <string>

Invoice::Invoice (int itemNumber, std::string description, int quantity, int price)
{
    setItemNumber(itemNumber);
    setDescription(description);
    setQuantity(quantity);
    setPrice(price);
}

void
Invoice::setItemNumber(int itemNumber)
{
    partNumber_ = itemNumber;
}

void
Invoice::setDescription(std::string description)
{
    partDescription_ = description;
}

void 
Invoice::setQuantity(int quantity)
{   
    if ( quantity < 0){

	std::cout << "Info 1: The quantity cannot have a negative value" << std::endl;

	return;	
    }   

    partQuantity_ = quantity; 
}

void
Invoice::setPrice(int price)
{	
    if (price < 0) {

	std::cout << "Info 2: The price cannot have negative value" << std::endl;

	return;
    }   

    partPrice_ = price;
}

int
Invoice::getItemNumber()
{
    return  partNumber_ ;
}

std::string
Invoice::getDescription()
{
    return partDescription_;
}

int
Invoice::getQuantity()
{
    return partQuantity_;
}

int
Invoice::getPrice()
{
    return partPrice_;
}

int 
Invoice::getInvoiceAmount()
{
    partAmount_ = partQuantity_ * partPrice_;

    return partAmount_;
}
