///Mikayel Ghaziyan
///10/10/2017
///Exercise 2.10


a)FALSE - It depends on the operator type. Some types read from left to right, while others from right to left.

b)TRUE - Variables names` can start with underscores, letters and etc. They cannot start with key terms that are used by the program, numbers or symbols.

c)FALSE - Here, "cout" prints out a chain of strings wrapped within the quotation marks. Any character or characters within these quotation marks are consideredstrings.Thus, it is not an assignment statement but a string line to be printed out on the screen.

d)FALSE - Some arithmetic operators have higher precedence. For example,the multiplication (*) or the division (/) have higher precedence than the sum (+) or the subtraction (-).

e)FALSE - the h22 is a valid variable name.    
