#include <iostream>
#include <string>

class Employee
{
public:
    Employee(std::string name, std::string surname, int salary);
    void setName(std::string name);
    std::string getName();
    void setSurname(std::string surname);
    std::string getSurname();
    void setSalary(int salary);
    int getSalary();

private:
    std::string name_;
    std::string surname_;
    int salary_;
};

