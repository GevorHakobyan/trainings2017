#include <iostream>

int
main()
{
    int count;
    std::cout << "Enter number of counts: ";
    std::cin >> count;

    if (count <= 0) {
        std::cerr << "Error 1. Entered number should be positive." << std::endl;
        return 1;
    }

    int sum = 0;
    for (int i = 1; i <= count; ++i) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;

        sum += number;
    }

    std::cout << "Sum: " << sum << std::endl;
    return 0;
}

