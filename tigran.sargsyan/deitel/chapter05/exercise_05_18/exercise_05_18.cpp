#include <iostream>

int
main()
{
    std::cout << "Decimal\t" << "Binary\t" << "Octal\t" << "Hexal"<< std::endl;
    for (int number = 1; number <= 256; ++number) {
        std::cout << number << "\t";
        for (int check = 2; check <= 16; check *= 2) {
            if (4 == check) {
                continue;
            }
            int counter = 1, current = number;
            while (counter <= number) {
                counter *= check;
            }
            counter /= check;
    	    for (counter; counter > 0; counter /= check) {
    	        int equivalent = current / counter;
    	        if (equivalent < 10){
    	            std::cout << equivalent;
    	        } else {
    	            int equivalentChar = equivalent + 55;
    	            std::cout << static_cast<char>(equivalentChar);
    	        }
    	        current = current % counter;
            }
            std::cout << "\t";
        }
        std::cout << std::endl;
    }
    return 0;
}

