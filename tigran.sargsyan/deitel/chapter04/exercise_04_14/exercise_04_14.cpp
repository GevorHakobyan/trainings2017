#include <iostream>

int
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Enter account number (-1 to quit): ";
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (accountNumber < 0) {
            std::cout << "Error 1. Account number should be positive." << std::endl;
            return 1;
        }

        double initialBalance;
        std::cout << "Enter beginning balance: ";
        std::cin >> initialBalance;
        if (initialBalance < 0) {
            std::cout << "Error 2. Initial balance should be positive." << std::endl;
            return 2;
        }

        double charges;
        std::cout << "Enter total charges: ";
        std::cin >> charges;
        if (charges < 0) {
            std::cout << "Error 3. Charges should be positive." << std::endl;
            return 3;
        }

        double credits;
        std::cout << "Enter total credits: ";
        std::cin >> credits;
        if (credits < 0) {
            std::cout << "Error 4. Credits should be positive." << std::endl;
            return 4;
        }

        double limit;
        std::cout << "Enter credit limit: ";
        std::cin >> limit;
        if (limit < 0) {
            std::cout << "Error 5. Limit should be positive." << std::endl;
            return 5;
        }

        double newBalance = initialBalance + charges - credits;
        std::cout << "New balance is " << newBalance << std::endl;

        if (newBalance > limit) {
            std::cout << "Account: " << accountNumber << std::endl;
            std::cout << "Credit limit " << limit << std::endl;
            std::cout << "Balance: " << newBalance << std::endl;
            std::cout << "Credit limit exceeded." << std::endl;
        }
    }

    return 0;
}

