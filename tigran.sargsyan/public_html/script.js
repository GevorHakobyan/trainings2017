$(document).ready(function() {
    $('.heading').on('click', function() {
        var content = '<div class="slide"></div>';
        $(this).parent().toggle("fold", 3000).promise().done(function(){
            $(this).replaceWith(content);
        });
        var number = 0;
        var timeId = setInterval(autoSlide, 10000);
        function autoSlide() {
            if (number >= 0 && number < 20) {
                ++number;
            } else {
                number = 0;
            }
            var address = 'url("images/slide_' + number + '.jpg")';
            $('.slide').css('background-image', address);
        }
    });
});
