#include <iostream>
#include <string>
#include "Date.hpp"

Date::Date(int year, int month, int day)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

void
Date::setYear(int year)
{
    year_ = year;
}

int
Date::getYear()
{
    return year_;
}

void
Date::setMonth(int month)
{
    if (month < 1) {
        month_ = 1;
        std::cout << "We are setting month to 1, because it can't be less than 1. " << std::endl;
        return;
    }
    if (month > 12) {
        month_ = 1;
        std::cout << "We are setting month to 1, because it can't be more than 12. " << std::endl;
        return;
    }
    
    month_ = month;
}

int
Date::getMonth()
{
    return month_;
}

void
Date::setDay(int day)
{
    day_ = day;
}

int
Date::getDay()
{
    return day_;
}
