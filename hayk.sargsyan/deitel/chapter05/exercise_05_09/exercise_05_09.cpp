#include <iostream>

int
main()
{
    int multiple = 1;

    for (int counter = 1; counter <= 15; counter += 2) {
        multiple *= counter;
    }

    std::cout << multiple << std::endl;

    return 0;
}
